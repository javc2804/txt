saveDoc = function(filename, data) {
  let blob = new Blob([data], {
    type: 'text/txt'
  });
  if (window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveBlob(blob, filename);
  } else {
    let elem = window.document.createElement('a');
    elem.href = window.URL.createObjectURL(blob);
    elem.download = filename;
    document.body.appendChild(elem);
    elem.click();
    document.body.removeChild(elem);
  }
}

//Aqui llamo a la funcion, el content es la variable que contiene lo que se quiere mostrar en el txt
saveDoc("file.txt", content);  